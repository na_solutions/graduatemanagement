/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import pojo.Absolvent;
import pojo.AbsolventBoolean;
import pojo.Jahrgang;
import pojo.Klasse;
import service.Service;

/**
 *
 * @author Bauer
 */
@ManagedBean
@SessionScoped
public class AdminBean {

    @ManagedProperty(value = "#{service}")
    private Service service;

    List<Klasse> kList = new ArrayList<>();
    List<Jahrgang> jList = new ArrayList<>();
    Map<String, Boolean> bmap = new HashMap<>();

    boolean svnrB;
    boolean vornameB;
    boolean nachnameB;
    boolean geschlechtB;
    boolean telefonB;
    boolean eMailB;
    boolean adresseB;
    boolean firmaB;
    boolean all;

    public AdminBean() {
        jList.add(new Jahrgang("2016/17"));
        /*  jList = service.getjList();
        jList.add(new Jahrgang("Jahrgang 2016/17"));
        kList = service.getjList().get(0).getkList();
        kList.add(new Klasse(1,"5AHIF","2016/17"));
        kList.add(new Klasse(2,"3AHIF","2015/16"));
        kList.add(new Klasse(3,"4AHIF","2016/17"));
        
        jList.get(0).setkList(kList);
         */
    }

    public void exportierenBoolean() {
        bmap.put("SVNR", false);
        bmap.put("NACHNAME", false);
        bmap.put("VORNAME", false);
        bmap.put("GESCHLECHT", false);
        bmap.put("TELEFONNUMMER", false);
        bmap.put("EMAIL", false);
        bmap.put("FIRMA", false);
        bmap.put("ADRESSE", false);
        if (all == true) {
            bmap.put("SVNR", true);
            bmap.put("FAMILIENNAME", true);
            bmap.put("VORNAME", true);
            bmap.put("GESCHLECHT", true);
            bmap.put("TELEFONNUMMER", true);
            bmap.put("EMAIL", true);
            bmap.put("FIRMA", true);
            bmap.put("ADRESSE", true);
        } else {
            if (svnrB == true) {
                bmap.put("SVNR", true);
            }
            if (this.nachnameB == true) {
                bmap.put("NACHNAME", true);
            }
            if (this.vornameB == true) {
                bmap.put("VORNAME", true);
            }
            if (this.geschlechtB == true) {
                bmap.put("GESCHLECHT", true);
            }
            if (this.telefonB == true) {
                bmap.put("TELEFONNUMMER", true);
            }
            if (this.eMailB == true) {
                bmap.put("EMAIL", true);
            }
            if (this.firmaB == true) {
                bmap.put("FIRMA", true);
            }
            if (this.adresseB == true) {
                bmap.put("ADRESSE", true);
            }
        }
    }

    public void exportieren() throws SQLException {

    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public List<Klasse> getkList() {
        return kList;
    }

    public void setkList(List<Klasse> kList) {
        this.kList = kList;
    }

    public List<Jahrgang> getjList() {
        return jList;
    }

    public void setjList(List<Jahrgang> jList) {
        this.jList = jList;
    }

    public boolean isSvnrB() {
        return svnrB;
    }

    public void setSvnrB(boolean svnrB) {
        this.svnrB = svnrB;
    }

    public boolean isVornameB() {
        return vornameB;
    }

    public void setVornameB(boolean vornameB) {
        this.vornameB = vornameB;
    }

    public boolean isNachnameB() {
        return nachnameB;
    }

    public void setNachnameB(boolean nachnameB) {
        this.nachnameB = nachnameB;
    }

    public boolean isGeschlechtB() {
        return geschlechtB;
    }

    public void setGeschlechtB(boolean geschlechtB) {
        this.geschlechtB = geschlechtB;
    }

    public boolean isTelefonB() {
        return telefonB;
    }

    public void setTelefonB(boolean telefonB) {
        this.telefonB = telefonB;
    }

    public boolean iseMailB() {
        return eMailB;
    }

    public void seteMailB(boolean eMailB) {
        this.eMailB = eMailB;
    }

    public boolean isAdresseB() {
        return adresseB;
    }

    public void setAdresseB(boolean adresseB) {
        this.adresseB = adresseB;
    }

    public boolean isFirmaB() {
        return firmaB;
    }

    public void setFirmaB(boolean firmaB) {
        this.firmaB = firmaB;
    }

    public boolean isAll() {
        return all;
    }

    public void setAll(boolean all) {
        this.all = all;
    }
}
