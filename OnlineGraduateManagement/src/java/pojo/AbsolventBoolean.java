/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Nikolaus
 */
public class AbsolventBoolean {
    
    
    
    Map<String,Boolean> absolventList;

    public AbsolventBoolean(List<Boolean> bList) {
        absolventList = new TreeMap<>();
        
        absolventList.put("svnr", bList.get(0));
        absolventList.put("vorname", bList.get(1));
        absolventList.put("nachname", bList.get(2));
        absolventList.put("geschlecht", bList.get(3));
        absolventList.put("telefon", bList.get(4));
        absolventList.put("eMail", bList.get(5));
        absolventList.put("beschäftigt", bList.get(6));
        absolventList.put("plz", bList.get(7));
        absolventList.put("straße", bList.get(8));
        absolventList.put("ort", bList.get((9)));
    }
    
    public boolean getBoolean(String key){
        return absolventList.get(key);
    }

    public Map<String, Boolean> getAbsolventList() {
        return absolventList;
    }

    public void setAbsolventList(Map<String, Boolean> absolventList) {
        this.absolventList = absolventList;
    }
    
    
    
    
    
}
