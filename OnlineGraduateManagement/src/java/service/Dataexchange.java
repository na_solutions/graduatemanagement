/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import pojo.Absolvent;

/**
 *
 * @author Nikolaus
 */
public class Dataexchange {

    public void export(Map<String, Boolean> bMap, Map<String, Absolvent> aMap, String fileLocation) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Absolventen");

        Set<String> keyset = aMap.keySet();   //ändern
        int rownum = 0;
        for (String key : keyset) {
            Row row = (Row) sheet.createRow(rownum++);
            Absolvent objArr = aMap.get(key); //ändern
            int cellnum = 0;
            if (bMap.get("SVNR")) {
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue(objArr.getSvNr());
            }
            if (bMap.get("NACHNAME")) {
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue(objArr.getNachname());
            }
            if (bMap.get("VORNAME")) {
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue(objArr.getVorname());
            }
            if (bMap.get("EMAIL")) {
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue(objArr.getEMail());
            }
            if (bMap.get("TELEFONNR")) {
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue(objArr.getTelefonNr());
            }
            if (bMap.get("FIRMA")) {
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue(objArr.getFirma());
            }
            if (bMap.get("ADRESSE")) {
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue(objArr.getOrtID());
                cell = row.createCell(cellnum++);
                cell.setCellValue(objArr.getStraße());
            }

        }

        try {
            FileOutputStream out
                    = new FileOutputStream(new File(fileLocation));

            workbook.write(out);
            out.close();

            System.out.println("Excel written successfully..");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    
    
    public void importExcel(String path) throws Exception {

        String filename = path;

        List sheetData = new ArrayList();

        FileInputStream fis = null;
        try {

            fis = new FileInputStream(filename);

            HSSFWorkbook workbook = new HSSFWorkbook(fis);

            HSSFSheet sheet = workbook.getSheetAt(0);

            Iterator rows = sheet.rowIterator();
            while (rows.hasNext()) {
                HSSFRow row = (HSSFRow) rows.next();
                Iterator cells = row.cellIterator();

                List data = new ArrayList();
                while (cells.hasNext()) {
                    HSSFCell cell = (HSSFCell) cells.next();
                    data.add(cell);
                }

                sheetData.add(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                fis.close();
            }
        }

        showExelData(sheetData);
    }

    private static void showExelData(List sheetData) {
        for (int i = 0; i < sheetData.size(); i++) {
            List list = (List) sheetData.get(i);
            for (int j = 0; j < list.size(); j++) {
                Cell cell = (Cell) list.get(j);
                if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                    System.out.print(cell.getNumericCellValue());
                } else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                    System.out.print(cell.getRichStringCellValue());
                } else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
                    System.out.print(cell.getBooleanCellValue());
                }
                if (j < list.size() - 1) {
                    System.out.print(", ");
                }
            }
            System.out.println("");
        }
    }
}
